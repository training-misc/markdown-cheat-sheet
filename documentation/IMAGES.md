# Images
This is the syntax for image
```
![Alt text](Image url "Image Sample")
```

### Image Sample
This is when the image is found. \
![Alt text](../assets/snapshot.png "Image Sample")


This is when the image cannot be found.\
![Image cannot be found!](/ "Image Sample")