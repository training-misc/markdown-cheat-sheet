# Backslash
This is the syntax for backlash in order to use a symbol.
```
Asteriks Symbol \*
Backslash Symbol \\
Backtick Symbol \`
Negative Symbol \-
Positive Symbol \+
```

###  Backslash Symbol Examples
- Asteriks  Symbol\ *
- Backslash Symbol \\ 
- Backtick Symbol ` 
- Negative Symbol \-
- Positibe Symbol \+