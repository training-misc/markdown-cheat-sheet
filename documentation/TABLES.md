# Tables
This is the syntax for using the table.
```
Alight Center
|Header 1| Header 2|
| :-:    | :-:     |
|Align Center| Align Center|

Align Left
|Header 1| Header 2|
| :-   | :-     |
|Align Left| Align Left|

Align Right
|Header 1| Header 2|
| -:    | -:     |
|Align Right| Align Right|
```

## Table Examples
### Alight Center
|Header 1| Header 2|
| :-:    | :-:     |
|Align Center| Align Center|
|Align Center| Align Center| 
|Align Center| Align Center| 

### Align Left
|Header 1| Header 2|
| :-   | :-     |
|Align Left| Align Left|
|Align Left| Align Left|
|Align Left| Align Left|

### Align Right
|Header 1| Header 2|
| -:    | -:     |
|Align Right| Align Right|
|Align Right| Align Right|
|Align Right| Align Right|